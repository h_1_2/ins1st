/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : ins1st

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 18/05/2019 14:22:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '数据表名',
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模块名',
  `biz_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '业务名',
  `create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='代码生成表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
BEGIN;
INSERT INTO `gen_table` VALUES (5, 'test_gen', 'test', 'cpp', '2019-05-16 13:21:20');
COMMIT;

-- ----------------------------
-- Table structure for sys_biz_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_biz_log`;
CREATE TABLE `sys_biz_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志标题',
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '记录参数',
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '执行类',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '执行方法',
  `create_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='业务日志';

-- ----------------------------
-- Records of sys_biz_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_biz_log` VALUES (40, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.controller.SysDictController', 'save', '2019-05-15 10:18:48');
INSERT INTO `sys_biz_log` VALUES (41, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.controller.SysDictController', 'save', '2019-05-15 10:19:05');
INSERT INTO `sys_biz_log` VALUES (42, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:22:25');
INSERT INTO `sys_biz_log` VALUES (43, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:22:42');
INSERT INTO `sys_biz_log` VALUES (44, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:23:37');
INSERT INTO `sys_biz_log` VALUES (45, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:24:57');
INSERT INTO `sys_biz_log` VALUES (46, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:25:29');
INSERT INTO `sys_biz_log` VALUES (47, '删除代码生成表', '{id:1,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'del', '2019-05-16 11:54:06');
INSERT INTO `sys_biz_log` VALUES (48, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:55:24');
INSERT INTO `sys_biz_log` VALUES (49, '删除代码生成表', '{id:2,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'del', '2019-05-16 11:55:27');
INSERT INTO `sys_biz_log` VALUES (50, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 11:58:59');
INSERT INTO `sys_biz_log` VALUES (51, '删除代码生成表', '{id:3,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'del', '2019-05-16 11:59:02');
INSERT INTO `sys_biz_log` VALUES (52, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 13:08:31');
INSERT INTO `sys_biz_log` VALUES (53, '删除代码生成表', '{id:4,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'del', '2019-05-16 13:08:34');
INSERT INTO `sys_biz_log` VALUES (54, '保存代码生成表', '{id:null,}', 'com.ins1st.modules.gen.table.controller.GenTableController', 'save', '2019-05-16 13:21:20');
INSERT INTO `sys_biz_log` VALUES (55, '删除业务日志', '{id:39,}', 'com.ins1st.modules.sys.log.controller.SysBizLogController', 'del', '2019-05-16 13:52:20');
INSERT INTO `sys_biz_log` VALUES (56, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.dict.controller.SysDictController', 'save', '2019-05-17 11:41:56');
INSERT INTO `sys_biz_log` VALUES (57, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.dict.controller.SysDictController', 'save', '2019-05-17 11:42:29');
INSERT INTO `sys_biz_log` VALUES (58, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.dict.controller.SysDictController', 'save', '2019-05-17 11:42:42');
INSERT INTO `sys_biz_log` VALUES (59, '保存字典表', '{id:null,}', 'com.ins1st.modules.sys.dict.controller.SysDictController', 'save', '2019-05-17 11:42:58');
INSERT INTO `sys_biz_log` VALUES (60, '更新字典表', '{id:74,}', 'com.ins1st.modules.sys.dict.controller.SysDictController', 'update', '2019-05-17 11:45:07');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `code` varchar(255) DEFAULT NULL COMMENT '值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict` VALUES (69, 1, 0, '性别', 'sex');
INSERT INTO `sys_dict` VALUES (70, 1, 69, '男', '1');
INSERT INTO `sys_dict` VALUES (71, 2, 69, '女', '2');
INSERT INTO `sys_dict` VALUES (72, 1, 0, 'html元素', 'htmlType');
INSERT INTO `sys_dict` VALUES (73, 2, 72, '输入框', '1');
INSERT INTO `sys_dict` VALUES (74, 3, 72, '下拉选择框', '2');
INSERT INTO `sys_dict` VALUES (75, 4, 72, '时间框', '3');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `p_id` int(11) DEFAULT NULL COMMENT '父级菜单',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单地址',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单权限',
  `is_menu` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是不是菜单',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标(只限一级菜单使用)',
  `sort` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单排序',
  `level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单层级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '#', '#', '0', '&#xe699;', '1', '1');
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', '/sys/sysUser/index', 'sys:user:index,sys:user:edit,sys:user:del', '1', '', '2', '2');
INSERT INTO `sys_menu` VALUES (3, 1, '菜单管理', '/sys/sysMenu/index', 'sys:menu:index', '1', NULL, '3', '2');
INSERT INTO `sys_menu` VALUES (4, 1, '角色管理', '/sys/sysRole/index', 'sys:role:index', '1', NULL, '4', '2');
INSERT INTO `sys_menu` VALUES (6, 1, '业务日志管理', '/sys/sysBizLog/index', 'sys:log:index,sys:log:del', '1', NULL, '5', '2');
INSERT INTO `sys_menu` VALUES (28, 0, '代码生成', '/table/genTable/index', 'gen:table:index,gen:table:add,gen:table:edit,gen:table:del', '1', '&#xe696;', '7', '1');
INSERT INTO `sys_menu` VALUES (29, 0, '数据源监控', '/druid/login.html', '#', '1', '&#xe6f4;', '8', '1');
INSERT INTO `sys_menu` VALUES (30, 0, '接口文档', '/swagger-ui.html', '#', '1', '&#xe70c;', '9', '1');
INSERT INTO `sys_menu` VALUES (31, 1, '字典管理', '/sys/sysDict/index', 'sys:dict:index,sys:dict:add,sys:dict:edit,sys:dict:del', '1', NULL, '10', '3');
INSERT INTO `sys_menu` VALUES (32, 2, '新增', '#', 'sys:user:add', '0', NULL, '1', '3');
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='通知表';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` varchar(255) DEFAULT NULL COMMENT '角色状态',
  `keyword` varchar(255) DEFAULT NULL COMMENT '角色标识',
  `sort` varchar(255) DEFAULT NULL COMMENT '角色排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '超级管理员', '1', 'admin1', '1');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='角色和菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (74, 1, 1);
INSERT INTO `sys_role_menu` VALUES (75, 1, 2);
INSERT INTO `sys_role_menu` VALUES (76, 1, 32);
INSERT INTO `sys_role_menu` VALUES (77, 1, 3);
INSERT INTO `sys_role_menu` VALUES (78, 1, 4);
INSERT INTO `sys_role_menu` VALUES (79, 1, 6);
INSERT INTO `sys_role_menu` VALUES (80, 1, 31);
INSERT INTO `sys_role_menu` VALUES (81, 1, 28);
INSERT INTO `sys_role_menu` VALUES (82, 1, 29);
INSERT INTO `sys_role_menu` VALUES (83, 1, 30);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名称',
  `user_nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `user_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户密码',
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户邮箱',
  `user_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户电话',
  `user_sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户性别',
  `role_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户角色',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户状态（1正常2冻结）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (4, 'admin', 'INS1ST贼牛逼', 'b4674fb71b5b62030be59814f75674c7814a89a424739272', '499719083@qq.com', '130000000', '1', '1,7', '1');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='用户和角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (58, 4, 1);
INSERT INTO `sys_user_role` VALUES (59, 4, 7);
INSERT INTO `sys_user_role` VALUES (60, 5, 1);
INSERT INTO `sys_user_role` VALUES (65, 6, 1);
COMMIT;

-- ----------------------------
-- Table structure for test_gen
-- ----------------------------
DROP TABLE IF EXISTS `test_gen`;
CREATE TABLE `test_gen` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `column_one` varchar(255) DEFAULT NULL COMMENT '字段1',
  `column_two` varchar(255) DEFAULT NULL COMMENT '字段2',
  `column_three` varchar(255) DEFAULT NULL COMMENT '字段3',
  `column_four` varchar(255) DEFAULT NULL COMMENT '字段4',
  `column_five` varchar(255) DEFAULT NULL COMMENT '字段5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试代码生成表';

SET FOREIGN_KEY_CHECKS = 1;
